# Debian Xfce Wallpapers

Colored and simple wallpapers I made for my Debian desktop running Xfce. Each wallaper is available as a landscape and as a portrait version, save a few that are only availabe as landscape (see the last thumbnails in the preview below). 

If you don't use Xfce as your desktop environment, or if you want to change anything else, feel free to edit the SVG source files (I made them in Inkscape). You can also download a ready to use PNG of each wallpaper (2560x1440).

![Preview](preview.jpg)
